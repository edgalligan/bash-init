#!/bin/bash

. /etc/lsb-release
if [ "$DISTRIB_CODENAME" == "qiana" ] || [ "$DISTRIB_CODENAME" == "rebecca" ]; then
    DISTRIB_CODENAME="trusty"
elif [ "$DISTRIB_CODENAME" == "maya" ]; then
    DISTRIB_CODENAME="precise"
elif [ "$DISTRIB_CODENAME" == "betsy" ]; then
    echo "Sorry this script isn't tested on Mint Betsy :/"
    exit 1
fi

SERVER_USERNAME=`whoami`
USERNAME=`who am i | awk '{print $1}'`
if [ "$SERVER_USERNAME" != 'root' ] || [ "$USERNAME" == 'root' ]; then
 echo "Please run this command with sudo"
 exit 1;
fi

MYSQLROOTPASSWORD="$1"
if [ -z "$MYSQLROOTPASSWORD" ]; then
    MYSQLROOTPASSWORD=`tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1`
    #read -p 'Enter a root password for your MySQL server: ' MYSQLROOTPASSWORD
fi

echo "[client]
user=root
password=$MYSQLROOTPASSWORD
" > /root/.my.cnf

debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQLROOTPASSWORD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQLROOTPASSWORD"

apt-get -y update

if [ "$DISTRIB_CODENAME" == "trusty" ]; then
    # Install basic stuff
    apt-get -y install git curl vim software-properties-common python-software-properties
else
    apt-get -y install git-core curl vim
fi

# Do bash + env stuff
echo 'TERM="xterm-color"' >> /etc/environment
echo "set tabstop=4
set shiftwidth=4
set expandtab
set modeline
let @h = 'i<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>    <meta charset=\"UTF-8\" />    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />    <title></title>    <link rel=\"stylesheet\" href=\"style.css\" />    <style>/*<![CDATA[*/ /*]]>*/</style></head><body>    <script src="script.js"></script>    <script>    /*<![CDATA[*/        /*]]>*/    </script></body></html>\'" >> /etc/skel/.vimrc
cp /etc/skel/.vimrc /root/
mkdir -p /root/.composer
echo '{
"http-basic": {},
    "github-oauth": {
        "github.com": "c5cf7e351cbe86450d8384555e3c7ff4122c1b17"}
}' >> "/root/.composer/auth.json"
USERDIRS=("/home/*/")
for USERDIR in $USERDIRS; do
    U=`echo $USERDIR | sed 's/^\/home\///' | sed 's/\/$//'`
    echo "Propagating new skel config files for user $U ..."
    
    # Vim
    cp /etc/skel/.vimrc $USERDIR
    chown $U:$U "/home/$U/.vimrc"
    
    # Composer
    mkdir -p "/home/$U/.composer"
    echo '{
    "http-basic": {},
    "github-oauth": {
        "github.com": "c5cf7e351cbe86450d8384555e3c7ff4122c1b17"}
}' >> "/home/$U/.composer/auth.json"
    chown -R $U:$U "/home/$U/.composer"
done

# Install AMP stack
apt-get -y install apache2 libapache2-mod-php5 apache2-utils php5-gd php5-mysql php5-ldap php5-cli php5-curl php-pear mysql-client mysql-server

# Install Composer PHP package-manager
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

if [ "$DISTRIB_CODENAME" == "trusty" ]; then 
    # Add PPAs (this command needs software-properties-common package)
    add-apt-repository -y ppa:rwky/nodejs-unstable
else
    echo "
deb http://ppa.launchpad.net/rwky/nodejs-unstable/ubuntu $DISTRIB_CODENAME main 
deb-src http://ppa.launchpad.net/rwky/nodejs-unstable/ubuntu $DISTRIB_CODENAME main" >> /etc/apt/sources.list
    gpg --keyserver keyserver.ubuntu.com --recv-keys 6BEA97CEAC3CA381594EFA2CDBB0271C5862E31D
    gpg --export --armor 6BEA97CEAC3CA381594EFA2CDBB0271C5862E31D | sudo apt-key add -
fi
apt-get -y update

# Install Node
apt-get -y install nodejs

USERCONF="<Directory /home/*/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>"

if [ -d "/etc/apache2/conf-available" ]; then
    echo "$USERCONF" > /etc/apache2/conf-available/user.conf
    a2enconf user
else
    echo "$USERCONF" > /etc/apache2/conf.d/user.conf
fi

service apache2 restart

echo "Server set up"



