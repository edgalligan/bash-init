#!/bin/bash

USERNAME="$1"
SITEALIAS="$2"
DOMAIN="$3"
ALLARGS=($@)
unset ALLARGS[0]
unset ALLARGS[1]
unset ALLARGS[2]
DOMAINALIASES=${ALLARGS[@]}
SERVER_USERNAME=`whoami`

if [ -z "$USERNAME" ]; then
    USERNAME=`who am i | awk '{print $1}'`
fi
if [ "$SERVER_USERNAME" != 'root' ]; then
    echo "Please run this command with sudo"
    exit 1;
fi
if [ "$USERNAME" == 'root' ]; then
    read -p "Enter the username of the owner of the new site: " USERNAME
fi
if [ -z "$SITEALIAS" ]; then
    read -p "Enter a short name for your new site: " SITEALIAS
fi
if [ -z "$DOMAIN" ]; then
    read -p "Enter the primary domain name for your new site: " DOMAIN
fi
if [ -z "$DOMAINALIASES" ]; then
    read -p "Enter a space-separated list of domain aliases: " DOMAINALIASES
fi

if [ ! id -u "$USERNAME" >/dev/null 2>&1 ]; then
    echo "user does not exist"
    exit 1;
fi

if [ ! -d "/home/$USERNAME/www/$SITEALIAS/public" ]; then
    mkdir -p /home/$USERNAME/www/$SITEALIAS/public
    mkdir -p /home/$USERNAME/www/$SITEALIAS/logs
    echo "Hello" > /home/$USERNAME/www/$SITEALIAS/public/index.html
fi

echo "<VirtualHost *:80>
        ServerName ${DOMAIN}
        ServerAlias ${DOMAINALIASES}

        ServerAdmin webmaster@localhost
        DocumentRoot /home/$USERNAME/www/$SITEALIAS/public

        ErrorLog /home/$USERNAME/www/$SITEALIAS/logs/error.log
        CustomLog /home/$USERNAME/www/$SITEALIAS/logs/access.log combined
</VirtualHost>" > /home/$USERNAME/www/$SITEALIAS/vhost.conf
echo "127.0.0.1 $DOMAIN" >> /etc/hosts

echo "IncludeOptional /home/$USERNAME/www/$SITEALIAS/vhost.conf" > /etc/apache2/sites-available/$SITEALIAS.conf
a2ensite $SITEALIAS

chown -R $USERNAME:$USERNAME /home/$USERNAME/www/

service apache2 restart

echo "New site set up at http://${DOMAIN}/"
